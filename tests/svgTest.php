<?php
/**
 * Test SVG images
 */

use PHPUnit\Framework\TestCase;

class SvgTest extends TestCase
{
    public function dimensionProvider() {
        return [
            [359, 109],
        ];
    }

    /**
     * @dataProvider dimensionProvider
     */
    public function testSvgImage($width, $height)
    {
        $string = \wee\SvgPlaceholder\Svg::image($width, $height);
        $this->assertTrue(strpos($string, '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="' . (int) $width . '" height="' . (int) $height . '" viewBox="0 0 ' . (int) $width . ' ' . (int) $height . '"') !== false);
    }

    /**
     * @dataProvider dimensionProvider
     */
    public function testSvgImageText($width, $height)
    {
        $string = \wee\SvgPlaceholder\Svg::image($width, $height);
        $this->assertTrue(strpos($string, '>' . (int) $width . '×' . (int) $height . '</text>') !== false);
    }

    /**
     * @dataProvider dimensionProvider
     */
    public function testSvgImageStyle($width, $height)
    {
        $string = \wee\SvgPlaceholder\Svg::image($width, $height);
        $this->assertTrue(strpos($string, '</style>') !== false);
        $this->assertTrue(strpos($string, '@font-face') !== false);
        $this->assertTrue(strpos($string, 'Overpass') !== false);
        $this->assertTrue(strpos($string, 'hsl(') !== false);
    }

    /**
     * @dataProvider dimensionProvider
     */
    public function testSvgImageSerif($width, $height)
    {
        $string = \wee\SvgPlaceholder\Svg::image($width, $height, 'serif');
        $this->assertTrue(strpos($string, 'Vollkorn') !== false);
    }

    /**
     * @dataProvider dimensionProvider
     */
    public function testSvgImageMonospace($width, $height)
    {
        $string = \wee\SvgPlaceholder\Svg::image($width, $height, 'monospace');
        $this->assertTrue(strpos($string, 'PT Mono') !== false);
    }

    /**
     * @dataProvider dimensionProvider
     */
    public function testSvgImageCursive($width, $height)
    {
        $string = \wee\SvgPlaceholder\Svg::image($width, $height, 'cursive');
        $this->assertTrue(strpos($string, 'Allura') !== false);
    }
}
