# SVG placeholder web site

Here you can find the source code for [svgplaceholder.wee.plus](https://svgplaceholder.wee.plus/). It is a service for SVG placeholder images.

## Requirements

- [PHP](http://php.net/) 8.0 or later.

## Software

- [Slim](https://www.slimframework.com/)
- [Twig](https://twig.symfony.com/)
- [meyfa/php-svg](https://packagist.org/packages/meyfa/php-svg)
- [vlucas/phpdotenv](https://packagist.org/packages/vlucas/phpdotenv)
- [Clean CSS](https://github.com/jakubpawlowicz/clean-css)

## Fonts

- [Overpass](http://overpassfont.org/), [SIL Open Font](http://scripts.sil.org/OFL) / [LGPL](http://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html) license
- [Vollkorn](http://vollkorn-typeface.com/), [SIL Open Font](http://scripts.sil.org/OFL) license
- [PT Mono](https://fonts.google.com/specimen/PT+Mono), [SIL Open Font](http://scripts.sil.org/OFL) license
- [Allura](https://fonts.google.com/specimen/Allura), [SIL Open Font](http://scripts.sil.org/OFL) license

## TODO

- Update to Slim 4.

## Copyright

[Walter Ebert](https://walter.ebert.engineering/)

## License

[MIT](https://spdx.org/licenses/MIT)
