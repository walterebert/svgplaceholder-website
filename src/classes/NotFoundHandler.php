<?php

/**
 * Page not found handler
 */

namespace wee\SvgPlaceholder;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Container;

class NotFoundHandler
{
    public static function response(Request $request, Response $response, Container $container)
    {
        $base_url = $request->getUri()->getBaseUrl();

        $responseHtml = $container['view']->render(
            $response,
            'page.twig',
            [
                'base_url' => $base_url,
                'pagetitle' => '404 Page not found',
                'css' => file_get_contents(__DIR__ . '/../../css/style.min.css'),
                'robots' => 'noindex,follow',
                'content' =>
                    '<h1>Page not found</h1>' . PHP_EOL .
                    '<p>Go to the <a href="/">homepage</a> for more information about this site.',
            ]
        );

        return $responseHtml
            ->withStatus(404);
    }
}
