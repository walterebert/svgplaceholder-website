<?php

/**
 * SVG handler
 */

namespace wee\SvgPlaceholder;

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Container;

class Svg
{
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, array $arguments)
    {
        // Set image properties
        $widthDefault = $width = 100;
        if (isset($arguments['width'])) {
            $width = abs((int) $arguments['width']);
        }
        if (!$width) {
            $width = $widthDefault;
        }
        $heightDefault = $height = 50;
        if (isset($arguments['height'])) {
            $height = abs((int) $arguments['height']);
        }
        if (!$height) {
            $height = $heightDefault;
        }
        $font = '';
        if (isset($arguments['font'])) {
            $font = (string) $arguments['font'];
        }

        $color = '';
        if (isset($arguments['color']) && self::isValidColor($arguments['color'])) {
            $color = '#' . ltrim($arguments['color'], '#');
        }

        $background = '';
        if (isset($arguments['background']) && self::isValidColor($arguments['background'])) {
            $background = '#' . ltrim($arguments['background'], '#');
        }

        // Add image
        $body = $response->getBody();
        $svg = self::image($width, $height, $font, $color, $background);
        $body->write($svg);

        // Set HTTP cache duration
        $response = $this->container->cache->allowCache(
            $response,
            'public',
            57 * random_int(3, 12)
        );

        // Return response object
        return $response
            ->withHeader(
                'Content-Type',
                'image/svg+xml'
            )
            ->withHeader(
                'Content-Disposition',
                'inline; filename=' . $width . 'x' . $height . '.svg'
            )
            ->withBody($body);
    }

    protected static function css(string $font, string $color = '', string $background = '#333')
    {
        // Select font
        $file_base = __DIR__ . '/../../fonts/';
        $font = strtolower($font);
        switch ($font) {
            case 'serif':
                $file = $file_base . 'vollkorn.min.css';
                $fontName = 'Vollkorn, serif';
                break;

            case 'monospace':
                $file = $file_base . 'pt-mono.min.css';
                $fontName = 'PT Mono, monospace';
                break;

            case 'cursive':
                $file = $file_base . 'allura.min.css';
                $fontName = 'Allura, cursive';
                break;

            default:
                $file = $file_base . 'overpass.min.css';
                $fontName = 'Overpass, sans-serif';
        }

        // Set colors
        if (self::isValidColor($font)) {
            $color = '#' . ltrim($font, '#');
        } elseif (!self::isValidColor($color)) {
            $hue = random_int(0, 359);
            $saturation = random_int(1, 30);
            $background = 'hsl(' . $hue . ', ' . $saturation . '%, ' . random_int(20, 50) . '%)';
            $color = 'hsl(' . $hue . ', ' . $saturation . '%, 85%)';
        }

        // Set CSS
        $fontFace = '';
        if (file_exists($file)) {
            $fontFace = file_get_contents($file);
        }
        $style = new \SVG\Nodes\Structures\SVGStyle(
            sprintf(
                '%ssvg{font-family:%s;color:#fff;background:%s;stroke:%s;fill:%s}',
                $fontFace,
                $fontName,
                $background,
                $color,
                $color
            )
        );

        return $style;
    }

    public static function image(int $width, int $height, string $font = '', $color = '', $background = '')
    {
        $fontSize = ceil($height / 3);
        if ($height / $width > .7) {
            $fontSize = ceil($width / 8);
        }

        // Create SVG document from font file
        $svg = new \SVG\SVG($width, $height);
        $document = $svg->getDocument();
        $document->setAttribute('viewBox', "0 0 $width $height");

        // Add CSS
        $css = self::css($font, $color, $background);
        $document->addChild($css);

        // Set text
        $svgText = self::text($width, $height, $fontSize);
        $document->addChild($svgText);

        // Return SVG image
        return $svg->toXMLString();
    }

    protected static function isValidColor(string $string)
    {
        if (preg_match('/^#?([0-9a-fA-F]{3}){1,2}$/', $string)) {
            return true;
        }

        return false;
    }

    protected static function text(int $width, int $height, int $fontSize)
    {
        $svgText = new \SVG\Nodes\Texts\SVGText();
        $svgText->setValue($width . '×' . $height);
        $svgText->setAttribute('x', '50%');
        $svgText->setAttribute('y', '50%');
        $svgText->setAttribute('font-size', $fontSize);
        $svgText->setAttribute('text-anchor', 'middle');
        $svgText->setAttribute('alignment-baseline', 'middle');
        $svgText->setAttribute('dominant-baseline', 'middle');

        return $svgText;
    }
}
