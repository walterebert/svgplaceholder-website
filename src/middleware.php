<?php

/**
 * Application middleware
 */

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

// Add caching
$app->add(new \Slim\HttpCache\Cache('public', 60));
