<?php

/**
 * Dependency injection configuration
 */

$container = $app->getContainer();

// Caching
$container['cache'] = function () {
    return new \Slim\HttpCache\CacheProvider();
};

// Twig templates
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(
        $container->get('settings')['renderer']['template_path'],
        ['cache' => $container->get('settings')['renderer']['cache']]
    );

    return $view;
};

//Override the default Not Found Handler
$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {
        return \wee\SvgPlaceholder\NotFoundHandler::response($request, $response, $container);
    };
};
