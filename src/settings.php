<?php

/**
 * Application settings
 */

return [
    'settings' => [
        'displayErrorDetails' => (bool) getenv('SLIM_DISPLAY_ERROR_DETAILS'),
        'renderer' => [
            'template_path' => __DIR__ . '/../templates',
            'cache' => getenv('SLIM_VIEW_CACHE') ? (string) getenv('SLIM_VIEW_CACHE') : false
        ],
    ],
];
