<?php

/**
 * Routes
 */

// SVG images
$app->get('/svg/{width}x{height}[/{font}[/{color}[/{background}[/]]]]', \wee\SvgPlaceholder\Svg::class);

// Pages
$app->get(
    '/[{page}[/]]',
    function (\Slim\Http\Request $request, \Slim\Http\Response $response, array $arguments) {
        $template = 'page';
        $pagetitle = '';
        $robots = '';

        $page = '';
        if (isset($arguments['page'])) {
            $page = trim($arguments['page'], '/');
        }

        // Set canonical URL
        $base_url = $request->getUri()->getBaseUrl();
        $canonical_url = $base_url . '/';
        if ($page) {
            $canonical_url .= $page . '/';
        }
        $canonical_url = str_replace('http://', 'https://', $canonical_url);

        // Page actions
        switch ($page) {
            case '':
                $template = 'home';
                $pagetitle = 'SVG Placeholder';
                break;

            case 'colophon':
            case 'privacy':
                $template = $page;
                $pagetitle = ucfirst($page);
                $robots = 'noindex,follow';
                break;

            default:
                // Page not found
                return \wee\SvgPlaceholder\NotFoundHandler::response($request, $response, $this);
        }

        // Return response object
        return $this->view->render(
            $response,
            $template . '.twig',
            [
                'base_url'      => $base_url,
                'canonical_url' => $canonical_url,
                'page'          => $page,
                'pagetitle'     => $pagetitle,
                'css'           => file_get_contents(__DIR__ . '/../css/style.min.css'),
                'robots'        => $robots,
            ]
        );
    }
);
